/*
Explicit matrix multiplication calling cblas.

Author: Vlad Niculae <vlad@vene.ro>
License: Simplified BSD
*/

#include <stdio.h>
#include <utils.h>
#include <assert.h>
#include <cblas.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	if(argc < 2) {
		printf("Usage: %s FILE_W_EXPLICIT FILE_X\n", argv[0]);
		return 1;
	}
	char* file_w_explicit = argv[1];
	char* file_x = argv[2];

	float *w_explicit, *x;
	int nin, nout, nsamples, nin_x;

	/* read input data */
	read_array(file_w_explicit, &w_explicit, &nout, &nin);
	read_array(file_x, &x, &nin_x, &nsamples);
	assert(nin_x == nin);

    float* result = (float*) calloc(nout * nsamples, sizeof(float));

    double start = clock();

    /* blas call: result = w_explicit * x */
    cblas_sgemm(
    	CblasRowMajor, 
    	CblasNoTrans,
    	CblasNoTrans, 
    	nout, 			  /* m: rows(w) */
    	nsamples,         /* n: cols(x) */
    	nin,              /* k: rows(x) */
    	1.0,              /* do not scale the product */ 
    	w_explicit,       /* W matrix */
    	nin,              /* stride of W: the n. of columns */
    	x,                /* X matrix */
    	nsamples,         /* stride of X: the n. of columns */
    	0.0,              /* forget what was in x */
    	result,           /* Y result matrix */
    	nsamples          /* stride of Y */
    );
    double finish = clock();
    fprintf(
    	stderr,
    	"time for Y(%d,%d) = W(%d,%d) X(%d,%d): %f s\n",
    	nout,
    	nsamples,
    	nout,
    	nin,
    	nin,
    	nsamples,
    	(finish - start) / CLOCKS_PER_SEC
    );

    /* output result */
    int i, j, k = 0;
    for(i = 0; i < nout; ++i) {
    	for(j = 0; j < nsamples; ++j)
    		printf("%.18e ", result[k++]);
    	printf("\n");
    }

    free(result);
    free(w_explicit);
    free(x);
	return 0;
}