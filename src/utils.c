#include <stdio.h>
#include <stdlib.h>

void read_array(const char* fname, float** out, unsigned long* nrow,
                unsigned long* ncol)
{
	char c;
	*nrow = 1;
	*ncol = 1;
	int i;

	FILE* f = fopen(fname, "r");

	while ((c = getc(f)) != '\n')
		if (c == ' ')
			++(*ncol);
	while ((c = getc(f)) != EOF)
		if (c == '\n')
			++(*nrow);

	unsigned long sz = *nrow * *ncol;
	*out = (float*) malloc(sz * sizeof(float));
	rewind(f);
	for (i = 0; i < sz; ++i) {
		fscanf(f, "%f", &(*out)[i]);
	}
}
/*
unsigned int hash(unsigned int i, unsigned int j, unsigned int nbuckets)
{
    unsigned long a = ((7681 + (i * (i + 3))) * 7681) % 786433;
    // printf("%d\n", a);
    a += j * (j + 3);
    // printf("%d\n", a);
    a = (a * 6151 + 3) % 786433;
    // printf("%d\n", a);
    a %= nbuckets;
    return (unsigned int) a;
}
*/

unsigned long hash(unsigned long i, unsigned long j, unsigned long nbuckets)
{
    unsigned long i_ = i << 1, j_ = j << 1;
    i_ += 3 * i;
    j_ += 3 * j;
    i_ *= 7681;
    i_ += 15286;
    i_ %= 786433;
    i_ += j_;

    i_ = ((i_ >> 16) ^ i_) * 0x45d9f3b;
    i_ = ((i_ >> 16) ^ i_) * 0x45d9f3b;
    i_ = ((i_ >> 16) ^ i_);
    return i_ % nbuckets;
}