// Thread block size
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <cuda.h>

#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128

extern "C" {
#include "utils.h"
}

extern void read_array(const char* fname, float** out, unsigned long* nrow,
                       unsigned long* ncol);

#define BLOCK_SIZE 32  /* Must divide matrix sizes for now!! */
//#define HSIZE 1999
#define HSIZE 10750 /* saturate gpu */
#define HASH_PER_THREAD ((HSIZE + (BLOCK_SIZE * BLOCK_SIZE) - 1)/(BLOCK_SIZE * BLOCK_SIZE))

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
//#define gpuErrchk(ans) {ans;}
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

// Matrices are stored in row-major order:
// M(row, col) = *(M.elements + row * M.stride + col)
typedef struct {
    unsigned long width;
    unsigned long height;
    unsigned long stride;
    float* elements;
} Matrix;


typedef struct {
    unsigned long width;
    unsigned long height;
    unsigned long hsize;
    float* elements;
} HashedMatrix;

// Get a matrix element
__device__ float GetElement(const Matrix A, int row, int col)
{
    return A.elements[row * A.stride + col];
}

/*
__device__ unsigned int dhash(unsigned int i, unsigned int j, unsigned int sz)
{
    unsigned long a = ((7681 + (i * (i + 3))) * 7681) % 786433;
    a += j * (j + 3);
    a = (a * 6151 + 3) % 786433;
    a %= sz;
    return (unsigned int) a;
}*/


__device__ unsigned long dhash(unsigned long i, unsigned long j, unsigned long
nbuckets)
{
    unsigned long i_ = i << 1, j_ = j << 1;
    i_ += 3 * i;
    j_ += 3 * j;
    i_ *= 7681;
    i_ += 15286;
    i_ %= 786433;
    i_ += j_;

    i_ = ((i_ >> 16) ^ i_) * 0x45d9f3b;
    i_ = ((i_ >> 16) ^ i_) * 0x45d9f3b;
    i_ = ((i_ >> 16) ^ i_);
    return i_ % nbuckets;
}

/*
__device__ float GetElement(const HashedMatrix A, int row, int col)
{
    //return A.elements[(row * A.width + col) % A.hsize];
    return A.elements[dhash(row, col, A.hsize)];
}
*/


// Set a matrix element
__device__ void SetElement(Matrix A, int row, int col, float value)
{
    A.elements[row * A.stride + col] = value;
}


// Get the BLOCK_SIZExBLOCK_SIZE sub-matrix Asub of A that is
// located col sub-matrices to the right and row sub-matrices down
// from the upper-left corner of A
 __device__ Matrix GetSubMatrix(Matrix A, int row, int col) 
{
    Matrix Asub;
    Asub.width    = BLOCK_SIZE;
    Asub.height   = BLOCK_SIZE;
    Asub.stride   = A.stride;
    Asub.elements = &A.elements[A.stride * BLOCK_SIZE * row
                                         + BLOCK_SIZE * col];
    return Asub;
}


// Forward declaration of the matrix multiplication kernel
__global__ void MatMulKernel(const HashedMatrix, const Matrix, Matrix);

// Matrix multiplication - Host code
// Matrix dimensions are assumed to be multiples of BLOCK_SIZE
void MatMul(const HashedMatrix A, const Matrix B, Matrix C)
{
    // Load A and B to device memory
    /*
    Matrix d_A;
    d_A.width = d_A.stride = A.width; d_A.height = A.height;
    size_t size = A.width * A.height * sizeof(float);
    cudaMalloc(&d_A.elements, size);
    cudaMemcpy(d_A.elements, A.elements, size,
               cudaMemcpyHostToDevice);
    */

    double t0, t1, t2, t3;

    t0 = clock();
    HashedMatrix d_A;
    d_A.width = A.width; d_A.height = A.height; d_A.hsize = A.hsize;
    gpuErrchk( cudaMalloc(&d_A.elements, d_A.hsize * sizeof(float)) );
    gpuErrchk( cudaMemcpy(d_A.elements, A.elements, d_A.hsize * sizeof(float),
                          cudaMemcpyHostToDevice) );
    
    Matrix d_B;
    d_B.width = d_B.stride = B.width; d_B.height = B.height;
    size_t size = B.width * B.height * sizeof(float);
    gpuErrchk( cudaMalloc(&d_B.elements, size) );
    gpuErrchk( cudaMemcpy(d_B.elements, B.elements, size,
                          cudaMemcpyHostToDevice) );

    // Allocate C in device memory
    Matrix d_C;
    d_C.width = d_C.stride = C.width; d_C.height = C.height;
    size = C.width * C.height * sizeof(float);
    gpuErrchk( cudaMalloc(&d_C.elements, size) );
    cudaDeviceSynchronize();
    // Invoke kernel
    // cudaDeviceReset();
    t1 = clock();
    dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    dim3 dimGrid((B.width + dimBlock.x - 1) / dimBlock.x,
                 (A.height + dimBlock.y - 1) / dimBlock.y);

    /*cudaFuncSetCacheConfig(MatMulKernel, cudaFuncCachePreferShared);*/
    MatMulKernel<<<dimGrid, dimBlock>>>(d_A, d_B, d_C);
    cudaDeviceSynchronize();
    t2 = clock();

    // Read C from device memory
    gpuErrchk( cudaMemcpy(C.elements, d_C.elements, size,
                          cudaMemcpyDeviceToHost) );
    gpuErrchk( cudaPeekAtLastError() );
    cudaDeviceSynchronize();
    t3 = clock();
    // Free device memory
    gpuErrchk( cudaFree(d_A.elements) );
    gpuErrchk( cudaFree(d_B.elements) );
    gpuErrchk( cudaFree(d_C.elements) );

    fprintf(
    	stderr,
    	"%d,%d,%d,%f,%f,%f\n",
    	A.height,
    	A.width,
    	B.width,
        (t1 - t0) / CLOCKS_PER_SEC,
        (t2 - t1) / CLOCKS_PER_SEC,
        (t3 - t2) / CLOCKS_PER_SEC);
}

// Matrix multiplication kernel called by MatMul()
 __global__ void MatMulKernel(HashedMatrix A, Matrix B, Matrix C)
{
    // Block row and column
    int blockRow = blockIdx.y;
    int blockCol = blockIdx.x;

    // Each thread block computes one sub-matrix Csub of C
    Matrix Csub = GetSubMatrix(C, blockRow, blockCol);

    // Each thread computes one element of Csub
    // by accumulating results into Cvalue
    float Cvalue = 0;

    // Thread row and column within Csub
    int row = threadIdx.y;
    int col = threadIdx.x;
    int id = row * BLOCK_SIZE + col;

    // copy A.elements to shared mem

    __shared__ float w[HSIZE];

    for(int k = 0; k < HASH_PER_THREAD; ++k) {
        if (id * HASH_PER_THREAD + k < HSIZE)
            w[id * HASH_PER_THREAD + k] = A.elements[id * HASH_PER_THREAD + k];
    }
    __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
    // Loop over all the sub-matrices of  B that are
    // required to compute Csub
    // Multiply each pair of sub-matrices together
    // and accumulate the results
    //printf("%d\n", A.width / BLOCK_SIZE);

    for (int m = 0; m < ((A.width + BLOCK_SIZE - 1) / BLOCK_SIZE); ++m) {
        // Get sub-matrix Asub of A
        //Matrix Asub = GetSubMatrix(A, blockRow, m);

        // Get sub-matrix Bsub of B
        Matrix Bsub = GetSubMatrix(B, m, blockCol);

        // Load Asub and Bsub from device memory to shared memory
        // Each thread loads one element of each sub-matrix


        //if (m * BLOCK_SIZE + row >= B.height) {
        //    //printf("%d %d\n", m, blockCol);
        //    Bs[row][col] = 0;
        //}
        //else
        Bs[row][col] = GetElement(Bsub, row, col);

        // Synchronize to make sure the sub-matrices are loaded
        // before starting the computation

        __syncthreads();

                /*for (int ff=0; ff < BLOCK_SIZE; ++ff)
                    printf("%d %d %f\n",
                    m * BLOCK_SIZE + e, blockCol *
                    BLOCK_SIZE + ff,
                    Bs[e][ff]);*/
        /*
        if (blockRow == 15 & blockCol == 0  & row >= 20)
            for (int e = 0; e < BLOCK_SIZE; ++e)
                printf("%d %f\n", m,
                Bs[e][col]);
        */


        ////if (((blockRow * BLOCK_SIZE + row) < A.height) &
        ////    ((blockCol * BLOCK_SIZE + col) < B.width))
        // Multiply Asub and Bsub together
        //float val;
            for (int e = 0; e < BLOCK_SIZE; ++e) {
                ////if (m * BLOCK_SIZE + e < A.width) {
                    Cvalue += Bs[e][col] * w[dhash(
                        blockRow * BLOCK_SIZE + row,
                        m * BLOCK_SIZE + e,
                        HSIZE)];
                ////}
            }
            
            /* debug
            if ((blockRow == 0 | blockCol == 0) & row == 0 & col == 0)
                printf("! W[%d %d] * Xs[%d][%d, %d]: %f %f %f\n",
                    blockRow * BLOCK_SIZE + row,
                    m * BLOCK_SIZE + e,
                    m, e, col,
                    GetElement(A, blockRow * BLOCK_SIZE + row, m * BLOCK_SIZE + e),
                    Bs[e][col],
                    val);
            */
        // Synchronize to make sure that the preceding
        // computation is done before loading two new
        // sub-matrices of A and B in the next iteration
        __syncthreads();
    }
    /* printf("%f", Cvalue); */
    SetElement(Csub, row, col, Cvalue); 
}

int main(int argc, char** argv) {
    if(argc < 3) {
        printf("Usage: %s FILE_W FILE_X n_out\n", argv[0]);
        return 1;
    }
    char* file_w = argv[1];
    char* file_x = argv[2];
    unsigned long nout = (unsigned long) strtol(argv[3], (char **) NULL, 10);

    unsigned long i, j, dummy;

    /* read input data */

    Matrix X; // = {4, 2, 2, (float[8]){3, 4, 5, 6, 7, 8, 9, 10}};
    read_array(file_x, &X.elements, &X.height, &X.width);
    X.stride = X.width;

    /*
    for(i = 0; i < X.height; ++i) {
        for(j = 0; j < X.width; ++j)
            printf("%.2e ", X.elements[X.stride * i + j]);
        printf("\n");
    }
    */

    HashedMatrix W;
    read_array(file_w, &W.elements, &W.hsize, &dummy);
    W.height = nout;
    W.width = X.height;

    assert(dummy == 1);

    // Declare output
    Matrix Y; // Y = WX
    Y.height = W.height;
    Y.width = X.width;
    Y.stride = Y.width;

    Y.elements = (float *) malloc(Y.height * Y.width * sizeof(float));

    MatMul(W, X, Y);
    /* output result */
    /*
    printf("Top left %f\n", Y.elements[0]);
    printf("Top right %f\n", Y.elements[Y.width - 1]);
    printf("Bottom left %f\n", Y.elements[Y.height * (Y.width - 1)]);
    printf("Bottom right %f\n", Y.elements[Y.height * Y.width - 1]);
    */
    #ifdef OUTPUT
    for(i = 0; i < Y.height; ++i) {
        for(j = 0; j < Y.width; ++j)
            printf("%.18e ", Y.elements[Y.stride * i + j]);
        printf("\n");
    }
    #endif


    free(X.elements);
    free(W.elements);
    free(Y.elements);
    fflush(stdin);
    return 0;
}
