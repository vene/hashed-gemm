/*
Explicit matrix multiplication calling cblas.

Author: Vlad Niculae <vlad@vene.ro>
License: Simplified BSD
*/

#include <stdio.h>
#include <utils.h>
#include <assert.h>
#include <cblas.h>
#include <time.h>
#include <stdlib.h>

#define LOOP_I for (i = 0; i < nout; ++i)
#define LOOP_J for (j = 0; j < nsamples; ++j)
#define LOOP_K for (k = 0; k < nin; ++k)

int main(int argc, char* argv[])
{
	if(argc < 3) {
		printf("Usage: %s FILE_W FILE_X n_out\n", argv[0]);
		return 1;
	}
	char* file_w = argv[1];
	char* file_x = argv[2];
    unsigned long nout = (unsigned long) strtol(argv[3], (char **) NULL, 10);

	float *w, *x;
	unsigned long nsamples, nin;
    unsigned long nbuckets, dummy;

	/* read input data */
	read_array(file_w, &w, &nbuckets, &dummy);
	read_array(file_x, &x, &nin, &nsamples);
	assert(dummy == 1);

    double t0, t1_alloc, t2_res;
    
    unsigned long i, j, k;
    
    t0 = clock();
    float* result = (float*) calloc(nout * nsamples, sizeof(float));
    t1_alloc = clock();

    // Order of loop matters!
    // With 500x500x1000, kij is BY FAR fastest, even beats BLAS, but all other permutations lose.

    //LOOP_K LOOP_I LOOP_J
    LOOP_K LOOP_I LOOP_J
    result[nsamples * i + j] += w[hash(i, k, nbuckets)] * x[nsamples * k + j];

    t2_res = clock();

    fprintf(
        stderr,
        "%ld,%ld,%ld,,%f,%f\n",
        nout,
        nin,
        nsamples,
        (t1_alloc - t0) / CLOCKS_PER_SEC,
        (t2_res - t1_alloc) / CLOCKS_PER_SEC
    );

    /* output result */
    #ifdef OUTPUT
    k = 0;
    for(i = 0; i < nout; ++i) {
    	for(j = 0; j < nsamples; ++j)
    		printf("%.18e ", result[k++]);
    	printf("\n");
    }
    #endif

    free(result);
    free(x);
    free(w);
	return 0;
}