/*
Explicit matrix multiplication calling cblas.

Author: Vlad Niculae <vlad@vene.ro>
License: Simplified BSD
*/
#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128 

#include <cublas_v2.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>

extern "C" {
#include "utils.h"
}

extern void read_array(const char* fname, float** out, unsigned long* nrow,
                       unsigned long* ncol);
extern unsigned long hash(unsigned long i, unsigned long j, unsigned long
nbuckets);

int main(int argc, char* argv[])
{
	if(argc < 3) {
		printf("Usage: %s FILE_W FILE_X n_out\n", argv[0]);
		return 1;
	}
	char* file_w = argv[1];
	char* file_x = argv[2];
    unsigned long nout = (unsigned long) strtol(argv[3], (char **) NULL, 10);

	float *w, *x;
	unsigned long nsamples, nin;
    unsigned long nbuckets, dummy;

	/* read input data */
	read_array(file_w, &w, &nbuckets, &dummy);
	read_array(file_x, &x, &nin, &nsamples);
	assert(dummy == 1);


    unsigned long i, j;
    double t0, t1_materialized, t2_gpu_cpy, t3_done, t4_res;

    t0 = clock();
    /* materialize w */
    float* w_materialized = (float*) malloc(nout * nin * sizeof(float));
    for(i = 0; i < nout; ++i)
        for(j = 0; j < nin; ++j)
            w_materialized[nin * i + j] = w[hash(i, j, nbuckets)];

    t1_materialized = clock();
    /* allocate on gpu */
    float *d_W, *d_X, *d_Y;
	cudaMalloc( &d_W, nout * nin * sizeof(float));
	cudaMalloc( &d_X, nin * nsamples * sizeof(float));
	cudaMalloc( &d_Y, nout * nsamples * sizeof(float));
	/* copy to gpu */
	cudaMemcpy(d_W, w_materialized, nout * nin * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_X, x, nin * nsamples * sizeof(float), cudaMemcpyHostToDevice);

    cudaDeviceSynchronize();
	t2_gpu_cpy = clock();
    
	/* cublas call */

	cublasHandle_t handle;

    /* Initialize CUBLAS */
    cublasStatus_t status = cublasCreate(&handle);
    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "!!!! CUBLAS initialization error\n");
    }
  
    float alpha = 1.0;
    float beta = 0.0;
    status = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
                         nsamples, nout, nin, 
                         &alpha, d_X, nsamples, 
                                 d_W, nin, 
                         &beta,  d_Y, nsamples); 
    if (status != CUBLAS_STATUS_SUCCESS) {
        fprintf(stderr, "!!!! kernel execution error.\n");
    }

    /* the following is explicit
    for (i = 0; i < nout; i++) {
        for (j = 0; j < nsamples; j++) {
            result[nsamples * i + j] = 0;
            for (int k = 0; k < nin_x; k++) {
                result[nsamples * i + j] +=
                    w_materialized[nin_x * i + k] * x[nsamples * k + j];
            }
        }
    }
    */

    cudaDeviceSynchronize();
    t3_done = clock();

    float* result = (float*) malloc(nout * nsamples * sizeof(float));
    cudaMemcpy(result, d_Y, nout * nsamples * sizeof(float), cudaMemcpyDeviceToHost);
    
    cudaFree(d_Y);
    cudaFree(d_W);
    cudaFree(d_X);

    cudaDeviceSynchronize();
    t4_res = clock();
    fprintf(
    	stderr,
    	"%ld,%ld,%ld,%f,%f,%f,%f\n",
    	nout,
    	nin,
    	nsamples,
        (t1_materialized - 	t0) / CLOCKS_PER_SEC,
        (t2_gpu_cpy - 		t1_materialized) / CLOCKS_PER_SEC,
        (t3_done - 			t2_gpu_cpy) / CLOCKS_PER_SEC,
        (t4_res - 			t3_done) / CLOCKS_PER_SEC
    );

    #ifdef OUTPUT
    /* output result */
    for(i = 0; i < nout; ++i) {
    	for(j = 0; j < nsamples; ++j)
    		printf("%.18e ", result[nsamples * i + j]);
    	printf("\n");
    }
    #endif

    free(result);
    free(w_materialized);
    free(x);
	return 0;
}