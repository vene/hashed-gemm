#ifndef HG_UTILS
#define HG_UTILS
//#define OUTPUT

void read_array(const char* fname, float** out,
                unsigned long* nrow, unsigned long* ncol);
unsigned long hash(unsigned long, unsigned long, unsigned long);

#endif