/*
Explicit matrix multiplication calling cblas.

Author: Vlad Niculae <vlad@vene.ro>
License: Simplified BSD
*/

#include <stdio.h>
#include <utils.h>
#include <assert.h>
#include <cblas.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    /* time the hash */
    /*
    int i, j;
    int d;
    double t;
    t = clock();
    for (i=0; i < 500; i++) for(j=0; j < 500; j++)
        d = (i + j) % 1999;  //hash(i, j, 1999);

    printf("%f\n", (clock() - t) / CLOCKS_PER_SEC);
    return 0; */

    /* test the hash */
    /*
    int zi, zj;
    double t;
    //t = clock();
    //for (zi=0; zi < 512; zi++) for(zj=0; zj < 512; zj++)
    //    printf("%d\n", hash(zi, zj, 10750));
    printf("%d\n", hash(511, 511, 10750));
    return 0;
    */
	if(argc < 3) {
		printf("Usage: %s FILE_W FILE_X n_out\n", argv[0]);
		return 1;
	}
	char* file_w = argv[1];
	char* file_x = argv[2];
    unsigned long nout = (unsigned long) strtol(argv[3], (char **) NULL, 10);

	float *w, *x;
	unsigned long nsamples, nin;
    unsigned long nbuckets, dummy;

	/* read input data */
	read_array(file_w, &w, &nbuckets, &dummy);
	read_array(file_x, &x, &nin, &nsamples);
	assert(dummy == 1);

    unsigned long i, j; // was int
    double t0, t1_mat_alloc, t2_materialized, t3_res_alloc, t4_res;

    t0 = clock();
    /* materialize w */
    float* w_materialized = (float*) malloc(nout * nin * sizeof(float));
    t1_mat_alloc = clock();
    for(i = 0; i < nout; ++i)
        for(j = 0; j < nin; ++j)
            w_materialized[nin * i + j] = w[hash(i, j, nbuckets)];

    t2_materialized = clock();
    float* result = (float*) malloc(nout * nsamples * sizeof(float));
    t3_res_alloc = clock();
    /* blas call: result = w_materialized * x */
    cblas_sgemm(
    	CblasRowMajor, 
    	CblasNoTrans,
    	CblasNoTrans, 
    	nout, 			  /* m: rows(w) */
    	nsamples,         /* n: cols(x) */
    	nin,              /* k: rows(x) */
    	1.0,              /* do not scale the product */ 
    	w_materialized,       /* W matrix */
    	nin,              /* stride of W: the n. of columns */
    	x,                /* X matrix */
    	nsamples,         /* stride of X: the n. of columns */
    	0.0,              /* forget what was in x */
    	result,           /* Y result matrix */
    	nsamples          /* stride of Y */
    );
    /* the following is explicit
    for (i = 0; i < nout; i++) {
        for (j = 0; j < nsamples; j++) {
            result[nsamples * i + j] = 0;
            for (int k = 0; k < nin_x; k++) {
                result[nsamples * i + j] +=
                    w_materialized[nin_x * i + k] * x[nsamples * k + j];
            }
        }
    }
    */
    t4_res = clock();
    
    fprintf(
    	stderr,
    	"%ld,%ld,%ld,%f,%f,%f,%f\n",
    	nout,
    	nin,
    	nsamples,
        (t1_mat_alloc - t0) / CLOCKS_PER_SEC,
        (t2_materialized - t1_mat_alloc) / CLOCKS_PER_SEC,
        (t3_res_alloc - t2_materialized) / CLOCKS_PER_SEC,
        (t4_res - t3_res_alloc) / CLOCKS_PER_SEC
    );

    /* output result */
    #ifdef OUTPUT
    for(i = 0; i < nout; ++i) {
    	for(j = 0; j < nsamples; ++j)
    		printf("%.18e ", result[nsamples * i + j]);
    	printf("\n");
    }
    #endif
    free(result);
    free(w_materialized);
    free(x);
	return 0;
}