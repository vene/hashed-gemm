NIN=500
NOUT=500

PREFIX=data/final_${NIN}_${NOUT}
W=${PREFIX}/w.txt
WMAT=${PREFIX}/W_materialized.txt

for k in 1 2 3 4; do
	echo $k;
	  #for n in 100 300 1000 3000 10000;
	  for n in 30000;
	  do
	     X=${PREFIX}/${n}_X.txt
	     SUF=${NIN}_${NOUT}_h1999_${n}.txt
	  	 bin/gemm_materialize $W $X ${NOUT} 2>>logs/cpumat_${SUF}
	     bin/gemm_onthefly $W $X ${NOUT} 2>>logs/cpuotf_${SUF}
	     optirun bin/cuda_materialize $W $X ${NOUT} 2>>logs/gpumat_${SUF}
	     optirun bin/cuda_onthefly $W $X ${NOUT} 2>>logs/gpuotf_${SUF}
	  done
done

