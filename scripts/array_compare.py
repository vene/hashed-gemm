# Author: Vlad Niculae <vlad@vene.ro>
# License: Simplified BSD

import numpy as np
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compare matrices A and B')
    parser.add_argument('fname_a', type=str)
    parser.add_argument('fname_b', type=str)
    args = parser.parse_args()

    A = np.loadtxt(args.fname_a)
    B = np.loadtxt(args.fname_b)
    print("Error:", np.sum((A - B) ** 2))