# Author: Vlad Niculae <vlad@vene.ro>
# License: Simplified BSD

import numpy as np
import argparse

# old version
#def myhash(i, j, n_buckets):
#    a = ((7681 + (i * (i + 3))) * 7681) % 786433
#    a += j * (j + 3)
#    a = (a * 6151 + 3) % 786433
#    a %= n_buckets
#    return a

allf = 0xFFFFFFFFFFFFFFFF
def myhash(i, j, n_buckets):
    i_ = i << 1
    i_ += 3 * i
    j_ = j << 1
    j_ += 3 * j
    i_ *= 7681
    i_ += 15286
    i_ %= 786433
    i_ += j_

    i_ = (((i_ >> 16) ^ i_) * 0x45d9f3b) & allf
    i_ = (((i_ >> 16) ^ i_) * 0x45d9f3b) & allf
    i_ = ((i_ >> 16) ^ i_) & allf
    #a = (i_ * 6151 + 3) % 786433
    #a %= n_buckets
    return i_ % n_buckets


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate hashed weights W, inputs X and outputs Y for testing.')
    parser.add_argument('-i', '--n_in', dest='n_in', type=int, required=True)
    parser.add_argument('-o', '--n_out', dest='n_out', type=int, required=True)
    # parser.add_argument('-n', '--n_samples', dest='n_samples',
    # default=10000, type=int)
    parser.add_argument('-b', '--n_buckets', dest='n_buckets', default=100, type=int)
    parser.add_argument('-p', '--out_path', dest='path', default='data/')
    parser.add_argument('-r', '--random_state', dest='random_state', default=0)
    args = parser.parse_args()
    # n_samples = args.n_samples
    n_in = args.n_in
    n_out = args.n_out
    n_buckets = args.n_buckets
    path = args.path
    rng = np.random.RandomState(args.random_state)

    # Y = WX implies X = (n_in, n_samples), W=(n_out, n_in), Y=(n_out, n_samples)
    # I would personally transpose all of them, but meh. This looks more mathy.

    w_data = np.random.randn(n_buckets).astype(np.float32)
    #bucket_indices = np.arange(n_out * n_in)  # indices into W.ravel
    #bucket_indices %= n_buckets
    bucket_indices = np.indices((n_out, n_in)).reshape(2, -1).astype(np.int64)
    bucket_indices = np.array([myhash(int(i), int(j), n_buckets=n_buckets)
                               for i, j in bucket_indices.T])
    np.savetxt(path + "hash.txt", bucket_indices)
    W = w_data[bucket_indices].reshape((n_out, n_in)).astype(np.float32)

    np.savetxt(path + 'w.txt', w_data)
    np.savetxt(path + 'W_materialized.txt', W)


    X = rng.randn(n_in, 1048567).astype(np.float32)
    Y = np.dot(W, X).astype(np.float32)
    for n_samples in [32 * 2 ** k for k in range(16)]:
    #for n_samples in (1, 3, 10, 30, 100, 300, 1000, 3000, 10000, 30000,
    #                  100000, 300000, 1000000):
        np.savetxt(path + '{}_X.txt'.format(n_samples), X[:, :n_samples])
        np.savetxt(path + '{}_Y.txt'.format(n_samples), Y[:, :n_samples])