#NIN=500
#NOUT=500

NIN=512
NOUT=512
HSZ=10750  # was 1999

mkdir data/${NIN}_${NOUT}
python scripts/generate_data.py -i ${NIN} -o ${NOUT} -b ${HSZ} -p data/${NIN}_${NOUT}/

# doing this in python now, no need to bash loop
#for s in 500 5000;
#do
# mkdir data/final_${s}_${s};
# for n in 1 3 10 30 100 300 1000 3000 10000 30000 100000;
# do
# 	~/conda/bin/python scripts/generate_data.py -i ${s} -o ${s} -n ${n} -b
#1999 -p data/final_${s}_${s}/${n}_
# done
#done

