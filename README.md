# Hashed GEMM

Efficient computation of Y = WX when W is stored implicitly as a small number of
parameters mapped to w<sub>ij</sub> through a hash function.
